package com.omodei.meetingmanagerapp.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class MeetingRoom {
	
	@Id
    @Column
	private String meetingRoomName;
	
	@Column
	private String location;
	
	@Column
	private int maxPlaces;
	
	/*@OneToMany(fetch = FetchType.LAZY, mappedBy = "meetingRoom")
	private Set<Meeting> meetings;*/

	public MeetingRoom() {
		this.meetingRoomName = null;
		this.location = null;
		this.maxPlaces = -1;
	}
	
	public MeetingRoom(String meetingRoomName, String location, int maxPlaces) {
		this.meetingRoomName = meetingRoomName;
		this.location = location;
		this.maxPlaces = maxPlaces;
	}

	public String getMeetingRoomName() {
		return meetingRoomName;
	}

	public String getLocation() {
		return location;
	}

	public int getMaxPlaces() {
		return maxPlaces;
	}

	/*@JsonIgnore
	public Set<Meeting> getMeetings() {
		return meetings;
	}*/
	
}
