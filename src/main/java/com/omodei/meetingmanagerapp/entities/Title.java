package com.omodei.meetingmanagerapp.entities;

public enum Title {
	
	MR("Mr"),
	MISS("Miss"),
	MRS("Mrs"),
	NO_TITLE("");

	private final String title;
	
	Title(String title) {
		this.title = title;
	}
	
	public String getTitle() {
		return title;
	}
	
	public String toString() {
		return title;
	}
	
}
