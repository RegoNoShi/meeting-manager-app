package com.omodei.meetingmanagerapp.controllers;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.omodei.meetingmanagerapp.entities.MeetingRoom;
import com.omodei.meetingmanagerapp.repositories.MeetingRoomRepository;

@RestController
@RequestMapping(value="/meetingRoom")
public class MeetingRoomsController {
	
	@Autowired
    private MeetingRoomRepository meetingRoomRepository;

	@RequestMapping(method = RequestMethod.GET)
    public List<MeetingRoom> getAllMeetings() {
        return StreamSupport.stream(meetingRoomRepository.findAll().spliterator(), false)
        		.collect(Collectors.toList());
    }
    
}
