package com.omodei.meetingmanagerapp.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.omodei.meetingmanagerapp.entities.Participant;

@Repository
public interface ParticipantRepository extends CrudRepository<Participant, Long> {
	
	Participant findByEmailAddress(String emailAddress);
	
}
