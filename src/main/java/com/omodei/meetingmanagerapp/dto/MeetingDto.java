package com.omodei.meetingmanagerapp.dto;

import java.time.LocalDate;
import java.time.LocalTime;

public class MeetingDto {

	private LocalDate date;
	private LocalTime startTime;
	private LocalTime endTime;
	private String subject;
	private String meetingRoomName;
	
	public MeetingDto() {
		this(null, null, null, null, null);
	}
	
	public MeetingDto(String subject, String meetingRoomName, LocalDate date, 
			LocalTime startTime, LocalTime endTime) {
		this.subject = subject;
		this.meetingRoomName = meetingRoomName;
		this.date = date;
		this.startTime = startTime;
		this.endTime = endTime;
	}

	public LocalDate getDate() {
		return date;
	}

	public LocalTime getStartTime() {
		return startTime;
	}

	public LocalTime getEndTime() {
		return endTime;
	}

	public String getSubject() {
		return subject;
	}

	public String getMeetingRoomName() {
		return meetingRoomName;
	}
	
	@Override
	public String toString() {
		return String.format("%s (When: %s from %s to %s, Where: %s)", subject, 
				date, startTime, endTime, meetingRoomName);
	}
	
}
