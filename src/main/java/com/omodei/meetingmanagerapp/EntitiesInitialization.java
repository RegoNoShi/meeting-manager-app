package com.omodei.meetingmanagerapp;

import java.time.LocalDate;
import java.time.LocalTime;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.omodei.meetingmanagerapp.entities.Meeting;
import com.omodei.meetingmanagerapp.entities.MeetingRoom;
import com.omodei.meetingmanagerapp.entities.Participant;
import com.omodei.meetingmanagerapp.repositories.MeetingRepository;
import com.omodei.meetingmanagerapp.repositories.MeetingRoomRepository;
import com.omodei.meetingmanagerapp.repositories.ParticipantRepository;

@Component
public class EntitiesInitialization {
	 
    @Autowired
    private MeetingRepository meetingRepository;
    
    @Autowired
    private MeetingRoomRepository meetingRoomRepository;
    
    @Autowired
    private ParticipantRepository participantRepository;
 
    @PostConstruct
    public void init() {    	
    	// Initialize meeting rooms
    	MeetingRoom mr1 = new MeetingRoom("Paris", "Tower A - 1st Floor", 20);
    	MeetingRoom mr2 = new MeetingRoom("Milan", "Tower A - 2nd Floor", 4);
    	MeetingRoom mr3 = new MeetingRoom("London", "Tower B - 3rd Floor", 8);
    	MeetingRoom mr4 = new MeetingRoom("Madrid", "Tower B - 1st Floor", 10);
    	MeetingRoom mr5 = new MeetingRoom("Lisboa", "Tower B - 1st Floor", 4);
    	meetingRoomRepository.save(mr1);
    	meetingRoomRepository.save(mr2);
    	meetingRoomRepository.save(mr3);
    	meetingRoomRepository.save(mr4);
    	meetingRoomRepository.save(mr5);
    	// Initialize sone people
    	Participant p1 = new Participant("m.rossi@gmail.com", "MR", "Mario", "Rossi");
    	Participant p2 = new Participant("l.bianchi@gmail.com", "MISS", "Laura", "Bianchi");
    	Participant p3 = new Participant("m.nera@gmail.com", "MR", "Marco", "Nera");
    	Participant p4 = new Participant("n.verdi@gmail.com", "MRS", "Naomi", "Verdi");
    	participantRepository.save(p1);
    	participantRepository.save(p2);
    	participantRepository.save(p3);
    	participantRepository.save(p4);
    	// Initialize one meeting
    	Meeting m1 = new Meeting("Hello World! meeting", mr3, LocalDate.now(), 
    			LocalTime.of(9, 00),LocalTime.of(13, 00));
    	m1.addParticipant(p1);
    	//m1.addParticipant(p2);
    	m1.addParticipant(p3);
    	m1.addParticipant(p4);
    	m1.excludeParticipant(p1);
    	m1.markAsAbsent(p4);
    	m1.newRandomPresenter();
    	//m1.setClosed(true);
    	meetingRepository.save(m1);
    }
    
}