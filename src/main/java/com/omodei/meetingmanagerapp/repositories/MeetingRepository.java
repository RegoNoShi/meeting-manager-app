package com.omodei.meetingmanagerapp.repositories;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.omodei.meetingmanagerapp.entities.Meeting;

@Repository
public interface MeetingRepository extends PagingAndSortingRepository<Meeting, Long> {
	
	List<Meeting> findByClosedOrderByDateDescStartTimeDescEndTimeDesc(boolean closed);
	
}
