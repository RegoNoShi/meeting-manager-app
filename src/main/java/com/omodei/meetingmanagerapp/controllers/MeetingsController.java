package com.omodei.meetingmanagerapp.controllers;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.omodei.meetingmanagerapp.dto.MeetingDto;
import com.omodei.meetingmanagerapp.entities.Meeting;
import com.omodei.meetingmanagerapp.entities.MeetingRoom;
import com.omodei.meetingmanagerapp.entities.Participant;
import com.omodei.meetingmanagerapp.repositories.MeetingRepository;
import com.omodei.meetingmanagerapp.repositories.MeetingRoomRepository;
import com.omodei.meetingmanagerapp.repositories.ParticipantRepository;

@RestController
@RequestMapping(value="/meeting")
public class MeetingsController {

	@Autowired
    private MeetingRepository meetingRepository;
	
	@Autowired
    private MeetingRoomRepository meetingRoomRepository;
	
	@Autowired
    private ParticipantRepository participantRepository;


	@RequestMapping(value="/search", method = RequestMethod.GET)
    public List<Meeting> getMeetings(@RequestParam(value = "closed") boolean closed) {
        return meetingRepository.findByClosedOrderByDateDescStartTimeDescEndTimeDesc(closed);
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<Meeting> getAllMeetings() {
        return StreamSupport.stream(meetingRepository.findAll().spliterator(), false)
        		.collect(Collectors.toList());
    }
    
    @Transactional
    @RequestMapping(value="/{meetingId}/presenter", method = RequestMethod.POST)
    public Participant changePresenter(@PathVariable(value = "meetingId") long meetingId) {
    	Meeting relatedMeeting = meetingRepository.findOne(meetingId);
    	if(relatedMeeting == null)
    		throw new UnknowMeetingIdException();
    	if(relatedMeeting.isClosed())
    		throw new ClosedMeetingException();
    	if(relatedMeeting.getParticipants().size() < 1)
    		throw new NoParticipantException();
    	Participant presenter = relatedMeeting.newRandomPresenter();
    	meetingRepository.save(relatedMeeting);
    	return presenter; 
    }
    
    @Transactional
    @RequestMapping(value="/{meetingId}/close", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public void closeMeeting(@PathVariable(value = "meetingId") long meetingId) {
    	Meeting relatedMeeting = meetingRepository.findOne(meetingId);
    	if(relatedMeeting == null)
    		throw new UnknowMeetingIdException();
    	if(relatedMeeting.isClosed())
    		throw new ClosedMeetingException();
    	relatedMeeting.setClosed(true);
    	meetingRepository.save(relatedMeeting);
    }
    
    @Transactional
    @RequestMapping(value="/{meetingId}/participant/{participantId}", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public void addParticipant(@PathVariable(value = "meetingId") long meetingId,
    		@PathVariable(value = "participantId") long participantId) {
    	Meeting relatedMeeting = meetingRepository.findOne(meetingId);
    	if(relatedMeeting == null)
    		throw new UnknowMeetingIdException();
    	if(relatedMeeting.isClosed())
    		throw new ClosedMeetingException();
    	Participant participant = participantRepository.findOne(participantId);
    	if(participant == null)
    		throw new UnknowParticipantIdException();
    	if(relatedMeeting.addParticipant(participant))
    		meetingRepository.save(relatedMeeting);
    }
    
    @Transactional
    @RequestMapping(value="/{meetingId}/exclude/{participantId}", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public void excludeParticipant(@PathVariable(value = "meetingId") long meetingId,
    		@PathVariable(value = "participantId") long participantId) {
    	Meeting relatedMeeting = meetingRepository.findOne(meetingId);
    	if(relatedMeeting == null)
    		throw new UnknowMeetingIdException();
    	if(relatedMeeting.isClosed())
    		throw new ClosedMeetingException();
    	Participant participant = participantRepository.findOne(participantId);
    	if(participant == null)
    		throw new UnknowParticipantIdException();
    	if(relatedMeeting.excludeParticipant(participant))
    		meetingRepository.save(relatedMeeting);
    }
    
    @Transactional
    @RequestMapping(value="/{meetingId}/absent/{participantId}", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public void absentParticipant(@PathVariable(value = "meetingId") long meetingId,
    		@PathVariable(value = "participantId") long participantId) {
    	Meeting relatedMeeting = meetingRepository.findOne(meetingId);
    	if(relatedMeeting == null)
    		throw new UnknowMeetingIdException();
    	Participant participant = participantRepository.findOne(participantId);
    	if(participant == null)
    		throw new UnknowParticipantIdException();
    	if(relatedMeeting.isClosed())
    		throw new ClosedMeetingException();
    	relatedMeeting.markAsAbsent(participant);
		meetingRepository.save(relatedMeeting);
    }
    
    @Transactional
    @RequestMapping(value="/{meetingId}/absent/{participantId}", method = RequestMethod.DELETE)
    @ResponseStatus(value = HttpStatus.OK)
    public void presentParticipant(@PathVariable(value = "meetingId") long meetingId,
    		@PathVariable(value = "participantId") long participantId) {
    	Meeting relatedMeeting = meetingRepository.findOne(meetingId);
    	if(relatedMeeting == null)
    		throw new UnknowMeetingIdException();
    	if(relatedMeeting.isClosed())
    		throw new ClosedMeetingException();
    	Participant participant = participantRepository.findOne(participantId);
    	if(participant == null)
    		throw new UnknowParticipantIdException();
    	if(relatedMeeting.unmarkAsAbsent(participant))
    		meetingRepository.save(relatedMeeting);
    }
    
    @Transactional
    @RequestMapping(value="/{meetingId}/exclude/{participantId}", method = RequestMethod.DELETE)
    @ResponseStatus(value = HttpStatus.OK)
    public void includeParticipant(@PathVariable(value = "meetingId") long meetingId,
    		@PathVariable(value = "participantId") long participantId) {	
    	Meeting relatedMeeting = meetingRepository.findOne(meetingId);
    	if(relatedMeeting == null)
    		throw new UnknowMeetingIdException();
    	if(relatedMeeting.isClosed())
    		throw new ClosedMeetingException();
    	Participant participant = participantRepository.findOne(participantId);
    	if(participant == null)
    		throw new UnknowParticipantIdException();
    	if(relatedMeeting.unexcludeParticipant(participant))
    		meetingRepository.save(relatedMeeting);
    }
    
    @RequestMapping(value="/{meetingId}/participant", method = RequestMethod.GET)
    public Set<Participant> getParticipants(@PathVariable(value = "meetingId") long meetingId) {
    	Meeting relatedMeeting = meetingRepository.findOne(meetingId);
    	if(relatedMeeting == null)
    		throw new UnknowMeetingIdException();
    	return relatedMeeting.getParticipants();
    }
    
    @RequestMapping(value="/{meetingId}/absent", method = RequestMethod.GET)
    public Set<Participant> getAbsents(@PathVariable(value = "meetingId") long meetingId) {
    	Meeting relatedMeeting = meetingRepository.findOne(meetingId);
    	if(relatedMeeting == null)
    		throw new UnknowMeetingIdException();
    	return relatedMeeting.getAbsents();
    }
    
    @RequestMapping(value="/{meetingId}/exclude", method = RequestMethod.GET)
    public Set<Participant> getExcluded(@PathVariable(value = "meetingId") long meetingId) {
    	Meeting relatedMeeting = meetingRepository.findOne(meetingId);
    	if(relatedMeeting == null)
    		throw new UnknowMeetingIdException();
    	return relatedMeeting.getExcluded();
    }
    
    @Transactional
    @RequestMapping(method = RequestMethod.POST)
    public void createNewMeeting(@RequestBody MeetingDto meetingDto) {
    	List<Meeting> opened = meetingRepository.findByClosedOrderByDateDescStartTimeDescEndTimeDesc(false);
    	if(!opened.isEmpty())
    		throw new MeetingAlreadyOpenedException();
    	Meeting meeting = validateMeeting(meetingDto);
        meetingRepository.save(meeting);
    }
    
    private Meeting validateMeeting(MeetingDto meetingDto) {
    	MeetingRoom meetingRoom = meetingRoomRepository.findOne(meetingDto.getMeetingRoomName());
    	if(meetingRoom == null)
    		throw new BadRequestException("Unknown meeting room");
    	String subject = meetingDto.getSubject();
    	if(subject == null || subject.isEmpty())
    		throw new BadRequestException("Missing or empty subject");
    	LocalDate date = meetingDto.getDate();
    	if(date == null || date.isBefore(LocalDate.now()))
    		throw new BadRequestException("Missing, invalid or past date");
    	LocalTime startTime = meetingDto.getStartTime(), endTime = meetingDto.getEndTime();
    	if(startTime == null)
    		throw new BadRequestException("Missing or invalid start time");
    	if(endTime == null)
    		throw new BadRequestException("Missing or invalid end time");
    	if(endTime.isBefore(startTime))
    		throw new BadRequestException("End time is before start time");
    	Meeting meeting = new Meeting(subject, meetingRoom, date, startTime, endTime);    	
    	return meeting;
    }
    
    @ResponseStatus(value = HttpStatus.NOT_ACCEPTABLE, 
    		reason = "No participant added to the meeting, unable to determine presenter")
    public class NoParticipantException extends RuntimeException {

		private static final long serialVersionUID = -2248860467378457822L;
		
    }
    
    @ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Invalid MeetingId")
    public class UnknowMeetingIdException extends RuntimeException {

    	private static final long serialVersionUID = 3263548965448862372L;
    	
    }
    
    @ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Invalid Meeting Room name")
    public class UnknowMeetingRoomException extends RuntimeException {

		private static final long serialVersionUID = -1262025422239655527L;
    	
    }
    
    @ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Invalid ParticipantId")
    public class UnknowParticipantIdException extends RuntimeException {

		private static final long serialVersionUID = -4155245084519617915L;

    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Invalid request")
    public class BadRequestException extends RuntimeException {   

		private static final long serialVersionUID = 3865334944002774251L;

		public BadRequestException(String message) {
    		super(message);
    	}
    }
    
    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Meeting is closed")
    public class ClosedMeetingException extends RuntimeException {

		private static final long serialVersionUID = -2591181408426292973L; 
		
    }
    
    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "There is already an open meeting")
    public class MeetingAlreadyOpenedException extends RuntimeException {

		private static final long serialVersionUID = 519886258165001101L;
    	
    }
    
}
