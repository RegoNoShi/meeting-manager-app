package com.omodei.meetingmanagerapp.entities;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Meeting {

	@Id
    @Column
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Long meetingId;
	
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
	@JoinTable(name = "meeting_participant", 
			joinColumns = {@JoinColumn(name = "meetingId")}, 
			inverseJoinColumns = {@JoinColumn(name = "participantId")})
	private Set<Participant> participants;
	
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
	@JoinTable(name = "meeting_absent", 
			joinColumns = {@JoinColumn(name = "meetingId")}, 
			inverseJoinColumns = {@JoinColumn(name = "participantId")})
	private Set<Participant> absents;
	
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
	@JoinTable(name = "meeting_excluded", 
			joinColumns = {@JoinColumn(name = "meetingId")}, 
			inverseJoinColumns = {@JoinColumn(name = "participantId")})
	private Set<Participant> excluded;
	
	@Column
	private LocalDate date;
	
	@Column
	private LocalTime startTime;
	
	@Column
	private LocalTime endTime;
	
	@Column
	private String subject;
	
	@Column
	private boolean closed;
	
	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
	@JoinColumn(name = "meetingRoomName", nullable = false)
	private MeetingRoom meetingRoom;
	
	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
	@JoinColumn(name = "participantId")
	private Participant presenter;
	
	public Meeting() {
		this(null, null, null, null, null);
	}
	
	public Meeting(String subject, MeetingRoom meetingRoom, Collection<Participant> participants,
			LocalDate date, LocalTime startTime, LocalTime endTime) {
		this(subject, meetingRoom, date, startTime, endTime);
		this.participants.addAll(participants);
	}
	
	public Meeting(String subject, MeetingRoom meetingRoom, LocalDate date
			, LocalTime startTime, LocalTime endTime) {
		this.subject = subject;
		this.meetingRoom = meetingRoom;
		this.participants = new HashSet<Participant>();
		this.absents = new HashSet<Participant>();
		this.excluded = new HashSet<Participant>();
		this.date = date;
		this.startTime = startTime;
		this.endTime = endTime;
		this.closed = false;
		this.meetingId = null;
	}

	public Long getMeetingId() {
		return meetingId;
	}

	public Set<Participant> getParticipants() {
		return participants;
	}

	public Set<Participant> getAbsents() {
		return absents;
	}
	
	public Set<Participant> getExcluded() {
		return excluded;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd")
	public LocalDate getDate() {
		return date;
	}

	@JsonFormat(pattern = "HH:mm:ss")
	public LocalTime getStartTime() {
		return startTime;
	}

	@JsonFormat(pattern = "HH:mm:ss")
	public LocalTime getEndTime() {
		return endTime;
	}

	public boolean isClosed() {
		return closed;
	}

	public String getSubject() {
		return subject;
	}

	public MeetingRoom getMeetingRoom() {
		return meetingRoom;
	}
	
	public Participant newRandomPresenter() {
		if(participants.size() < 1)
			return null;
		int presenterId = new Random().nextInt(participants.size());
		Iterator<Participant> participantIterator =  participants.iterator();
		for(int i = 0; i < presenterId; i++)
			participantIterator.next();
		presenter = participantIterator.next();
		return presenter;
	}

	public Participant getPresenter() {
		return presenter;
	}
	
	public void setClosed(boolean closed) {
		this.closed = closed;
	}

	public boolean addParticipant(Participant participant) {
		return this.participants.add(participant);
	}
	
	public boolean excludeParticipant(Participant participant) {
		if(!participants.contains(participant))
			return false;
		participants.remove(participant);
		if(participant.equals(presenter))
			presenter = null;
		return excluded.add(participant);
	}
	
	public boolean unexcludeParticipant(Participant participant) {
		if(!excluded.contains(participant))
			return false;
		excluded.remove(participant);
		return participants.add(participant);
	}
	
	public boolean markAsAbsent(Participant participant) {
		if(!participants.contains(participant))
			return false;
		participants.remove(participant);
		if(participant.equals(presenter))
			presenter = null;
		return absents.add(participant);
	}
	
	public boolean unmarkAsAbsent(Participant participant) {
		if(!absents.contains(participant))
			return false;
		absents.remove(participant);
		return participants.add(participant);
	}
	
	@Override
	public String toString() {
		return String.format("%d - %s (When: %s from %s to %s, Where: %s)", meetingId , subject, 
				date, startTime, endTime, meetingRoom);
	}

	@Override
	public int hashCode() {
		if(meetingId == null)
			return 0;
		return meetingId.intValue();
	}

	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof Meeting))
			return false;
		Meeting that = (Meeting)obj;
		return this.meetingId.equals(that.meetingId);
	}
	
}
