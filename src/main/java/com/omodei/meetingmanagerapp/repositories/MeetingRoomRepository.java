package com.omodei.meetingmanagerapp.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.omodei.meetingmanagerapp.entities.MeetingRoom;

@Repository
public interface MeetingRoomRepository extends CrudRepository<MeetingRoom, String> {

}
