package com.omodei.meetingmanagerapp.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Participant {
	 
	@Id
    @Column
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long participantId;
    
	@Column(unique = true)
	private String emailAddress;
	
	@Column
	private Title title;

	@Column
	private String firstName;

	@Column
	private String lastName;

	public Participant() {
	}
	
	public Participant(String emailAddress, String title, String firstName, String lastName) {
		this(emailAddress, Title.valueOf(title), firstName, lastName);
	}
	
	public Participant(Long participantId, String emailAddress, Title title,
			String firstName, String lastName) {
		this(emailAddress, title, firstName, lastName);
		this.participantId = participantId;
	}
	
	public Participant(String emailAddress, Title title, String firstName, String lastName) {
		this.emailAddress = emailAddress;
		this.title = title;
		this.firstName = firstName;
		this.lastName = lastName;
		this.participantId = null;
	}
	
	public String getEmailAddress() {
		return emailAddress;
	}

	public String getTitle() {
		return title.toString();
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public Long getParticipantId() {
		return participantId;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public void setTitle(Title title) {
		this.title = title;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public void setParticipantId(Long participantId) {
		this.participantId = participantId;
	}
	
	@Override
	public int hashCode() {
		if(participantId == null)
			return 0;
		return participantId.intValue();
	}
	
	@Override
	public String toString() {
		return participantId + " # " + title + " " + firstName + " " + lastName 
				+ " (" + emailAddress + ")";
	}

	@Override
	public boolean equals(Object obj) {
		if(this == obj)
			return true;
		if(!(obj instanceof Participant))
			return false;
		Participant that = (Participant)obj;
		return participantId.equals(that.participantId);
	}
	
}
