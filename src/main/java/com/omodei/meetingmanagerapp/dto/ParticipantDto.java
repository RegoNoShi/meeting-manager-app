package com.omodei.meetingmanagerapp.dto;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ParticipantDto {
	
	private static final Pattern VALID_EMAIL_ADDRESS_REGEX = 
		    Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

	private String emailAddress;
	
	private String title;
	
	private String firstName;
	
	private String lastName;
	
	public String getEmailAddress() {
		return emailAddress;
	}
	
	public String getTitle() {
		return title;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public static boolean validate(String emailStr) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX .matcher(emailStr);
        return matcher.find();
	}
	
}
