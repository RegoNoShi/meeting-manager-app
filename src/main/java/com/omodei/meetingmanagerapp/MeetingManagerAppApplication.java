package com.omodei.meetingmanagerapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MeetingManagerAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(MeetingManagerAppApplication.class, args);
	}
}
