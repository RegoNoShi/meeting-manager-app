# Meeting Manager App

## Compile, Build and Launch 
JDK 8 required and JAVA_HOME must be set  
  
mvnw spring-boot:run (or mvn if already installed)  
  
Navigate to http://localhost:8080 with your favorite browser  

## Other
Compile with mvnw compile (or mvn if already installed)  
  
Build package with mvnw package (or mvn if already installed)  
  
Launch with java -jar target\meeting-manager-app-1.0.jar  
  