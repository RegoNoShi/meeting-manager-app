var people = {};
var participants = {};
var absents = {};
var excluded = {};
var meetingId = -1;

function participantToStr(participant) {
	var str = participant.emailAddress;
	var desc = "";
	if(participant.title)
		desc += participant.title;
	if(participant.firstName)
		desc += (desc ? " " : "") + participant.firstName;
	if(participant.lastName)
		desc += (desc ? " " : "") + participant.lastName;
	return str + (desc ? " (" + desc + ")" : "");
}

function loadPeople() {
	$('#people').empty();
	people = {}
	$.ajax({
		type: 'GET',
		cache: false,
        url: '/participant',
        async: false
    }).done(function(data) {
		$.each(data, function(key, val) {
			people[val.participantId] = val;
		});
	});
}

function loadParticipants() {
	$('#participants').empty();
	participants = {};
	$.ajax({
		type: 'GET',
		cache: false,
        url: '/meeting/' + meetingId + '/participant',
        async: false
    }).done(function(data) {
		$.each(data, function(key, val) {
			delete people[val.participantId];
			participants[val.participantId] = val;
			$('#participants').append(
					$('<option>').attr("value", val.participantId).text(participantToStr(val))
			);
		});
	});
}

function loadAbsents() {
	$('#absents').empty();
	absents = {};
	$.ajax({
		type: 'GET',
		cache: false,
        url: '/meeting/' + meetingId + '/absent',
        async: false
    }).done(function(data) {
		$.each(data, function(key, val) {
			delete people[val.participantId];
			absents[val.participantId] = val;
			$('#absents').append(
					$('<option>').attr("value", val.participantId).text(participantToStr(val))
			);
		});
	});
}

function loadExcluded() {
	$('#excluded').empty();
	excluded = {};
	$.ajax({
		type: 'GET',
		cache: false,
        url: '/meeting/' + meetingId + '/exclude',
        async: false
    }).done(function(data) {
		$.each(data, function(key, val) {
			delete people[val.participantId];
			excluded[val.participantId] = val;
			$('#excluded').append(
					$('<option>').attr("value", val.participantId).text(participantToStr(val))
			);
		});
	});
}

function loadManageParticipantsData(meetingIdParam) {
	meetingId = meetingIdParam;
	loadPeople();
	loadParticipants();
	loadAbsents();
	loadExcluded();
	$('#people').empty();
	$.each(people, function(key, val) {
		$('#people').append(
				$('<option>').attr("value", val.participantId).text(participantToStr(val))
		);
	});
}

function addClickListener() {
	$('#people2participants').click(function() {
		var people = $('#people').val();
		$.each(people, function(key, val) {
			$.ajax({
				type: 'POST',
				cache: false,
				url: '/meeting/' + meetingId + '/participant/' + val
			}).done(function(data) {
				$("#people option[value='" + val + "']").remove()
						.prop('selected' , false).appendTo($('#participants'));
		  	}).fail(function(data) {
		  		alert(JSON.stringify(data));
		  	});
		});
	});
	$('#participants2excluded').click(function() {
		var participants = $('#participants').val();
		$.each(participants, function(key, val) {
			$.ajax({
				type: 'POST',
				cache: false,
				url: '/meeting/' + meetingId + '/exclude/' + val
			}).done(function(data) {
				$("#participants option[value='" + val + "']").remove()
						.prop('selected' , false).appendTo($('#excluded'));
		  	}).fail(function(data) {
		  		alert(JSON.stringify(data));
		  	});
		});
	});
	$('#participants2absents').click(function() {
		var participants = $('#participants').val();
		$.each(participants, function(key, val) {
			$.ajax({
				type: 'POST',
				cache: false,
				url: '/meeting/' + meetingId + '/absent/' + val
			}).done(function(data) {
				$("#participants option[value='" + val + "']").remove()
						.prop('selected' , false).appendTo($('#absents'));
		  	}).fail(function(data) {
		  		alert(JSON.stringify(data));
		  	});
		});
	});
	$('#absents2participants').click(function() {
		var absents = $('#absents').val();
		$.each(absents, function(key, val) {
			$.ajax({
				type: 'DELETE',
				cache: false,
				url: '/meeting/' + meetingId + '/absent/' + val
			}).done(function(data) {
				$("#absents option[value='" + val + "']").remove()
						.prop('selected' , false).appendTo($('#participants'));
		  	}).fail(function(data) {
		  		alert(JSON.stringify(data));
		  	});
		});
	});
	$('#excluded2participants').click(function() {
		var excluded = $('#excluded').val();
		$.each(excluded, function(key, val) {
			$.ajax({
				type: 'DELETE',
				cache: false,
				url: '/meeting/' + meetingId + '/exclude/' + val
			}).done(function(data) {
				$("#excluded option[value='" + val + "']").remove()
						.prop('selected' , false).appendTo($('#participants'));
		  	}).fail(function(data) {
		  		alert(JSON.stringify(data));
		  	});
		});
	});
	$('#createPerson').click(function() {
		var jsonData = {};
		var errorText = null;
		$('#errorParticipant').empty();
		jsonData['emailAddress'] = $('#emailAddress').val();
		if(jsonData['emailAddress'] == null || jsonData['emailAddress'].length == 0) {
			$('#errorParticipant').text('Missing email address');
			return;
		}
		jsonData['firstName'] = $('#firstName').val();
		jsonData['lastName'] = $('#lastName').val();
		jsonData['title'] = $('#title').val();
		$.ajax({
			type: 'POST',
			cache: false,
			url: '/participant',
			data: JSON.stringify(jsonData),
			contentType: "application/json;charset=UTF-8"
		}).done(function(data) {
			$('#addParticipant')[0].reset();
			loadManageParticipantsData(meetingId);
	  	}).fail(function(data) {
	  		$('#errorParticipant').text(data.responseJSON.message);
	  		console.log(data);
	  	});
	});
}