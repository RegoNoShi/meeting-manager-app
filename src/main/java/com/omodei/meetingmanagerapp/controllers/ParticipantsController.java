package com.omodei.meetingmanagerapp.controllers;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.omodei.meetingmanagerapp.dto.ParticipantDto;
import com.omodei.meetingmanagerapp.entities.Participant;
import com.omodei.meetingmanagerapp.entities.Title;
import com.omodei.meetingmanagerapp.repositories.ParticipantRepository;

@RestController
@RequestMapping(value="/participant")
public class ParticipantsController {

	@Autowired
    private ParticipantRepository participantRepository;

    @RequestMapping(method = RequestMethod.GET)
    public List<Participant> getAllParticipants() {
        return StreamSupport.stream(participantRepository.findAll().spliterator(), false)
        		.collect(Collectors.toList());
    }
    
    @Transactional
    @RequestMapping(method = RequestMethod.POST)
    public void createNewParticipant(@RequestBody ParticipantDto participantDto) {
    	String emailAddress = participantDto.getEmailAddress();
    	if(emailAddress == null || emailAddress.isEmpty())
    		throw new MissingEmailAddressException();
    	if(!ParticipantDto.validate(emailAddress))
    		throw new InvalidEmailAddressException();
    	if(participantRepository.findByEmailAddress(emailAddress) != null)
    		throw new ParticipantAlreadyExistsException();
    	Participant participant = new Participant(participantDto.getEmailAddress(), 
    			participantDto.getTitle(), participantDto.getFirstName(), participantDto.getLastName());
        participantRepository.save(participant);
    }
    
    @Transactional
    @RequestMapping(value="/{participantId}", method = RequestMethod.DELETE)
    public void removeParticipant(@PathVariable(value = "participantId") long participantId) {
    	if(!participantRepository.exists(participantId))
    		throw new UnknownParticipantIdException();
    	participantRepository.delete(participantId);
    }
    
    @Transactional
    @RequestMapping(value="/{participantId}", method = RequestMethod.PATCH)
    public void editParticipant(@PathVariable(value = "participantId") long participantId, 
    		@RequestBody ParticipantDto participantDto) {
    	Participant participant = participantRepository.findOne(participantId);
    	if(participant == null)
    		throw new UnknownParticipantIdException();
    	String emailAddress = participantDto.getEmailAddress();
    	if(emailAddress != null && !emailAddress.isEmpty() &&
    			!emailAddress.equals(participant.getEmailAddress())) {
    		if(participantRepository.findByEmailAddress(emailAddress) != null)
        		throw new ParticipantAlreadyExistsException();
    		participant.setEmailAddress(participantDto.getEmailAddress());
    	}
    	if(participantDto.getTitle() != null)
    		participant.setTitle(Title.valueOf(participantDto.getTitle()));
    	if(participantDto.getFirstName() != null)
    		participant.setFirstName(participantDto.getFirstName());
    	if(participantDto.getLastName() != null)
    		participant.setLastName(participantDto.getLastName());
    	participantRepository.save(participant);
    }
    
    @Transactional
    @RequestMapping(value="/{participantId}", method = RequestMethod.PUT)
    public void replaceParticipant(@Valid @PathVariable(value = "participantId") long participantId, 
    		@RequestBody ParticipantDto participantDto) {
    	Participant participant = participantRepository.findOne(participantId);
    	if(participant == null)
    		throw new UnknownParticipantIdException();
    	String emailAddress = participantDto.getEmailAddress();
    	if(emailAddress != null && !emailAddress.isEmpty() &&
    			!emailAddress.equals(participant.getEmailAddress())) {
    		if(participantRepository.findByEmailAddress(emailAddress) != null)
        		throw new ParticipantAlreadyExistsException();
    	} else
    		throw new MissingEmailAddressException();
    	participant.setEmailAddress(emailAddress);
    	participant.setTitle(Title.valueOf(participantDto.getTitle()));
    	participant.setFirstName(participantDto.getFirstName());
    	participant.setLastName(participantDto.getLastName());
    	participantRepository.save(participant);
    }
    
    @ResponseStatus(value = HttpStatus.NOT_ACCEPTABLE, 
    		reason = "Email address already present")
    public class ParticipantAlreadyExistsException extends RuntimeException {

		private static final long serialVersionUID = -6908054625930967606L;

    }
    
    @ResponseStatus(value = HttpStatus.NOT_ACCEPTABLE, 
    		reason = "Invalid email address")
    public class InvalidEmailAddressException  extends RuntimeException {

		private static final long serialVersionUID = -8705398681911877748L;
    	
    }
    
    @ResponseStatus(value = HttpStatus.NOT_ACCEPTABLE, 
    		reason = "Email address is mandatory")
    public class MissingEmailAddressException extends RuntimeException {
    	
		private static final long serialVersionUID = 5052506199630184073L;

    }
    
    @ResponseStatus(value = HttpStatus.NOT_FOUND, 
    		reason = "Invalid participantId")
    public class UnknownParticipantIdException extends RuntimeException {

		private static final long serialVersionUID = 8016373788774492868L;
		
    }
	
}
