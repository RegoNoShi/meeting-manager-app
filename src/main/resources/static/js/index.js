
$(function() { 
	loadData();
});

function loadData() {
	loadClosed();
	loadOpened();
}

function loadClosed() {
	$.ajax({
		type: 'GET',
		cache: false,
		url: '/meeting/search?closed=true'
	}).done(function(data) {
		$(".btn").mouseup(function(){
		    $(this).blur();
		})
		$('#closed').empty();
		$('#closed').append(
				$('<div>').attr("class", "row").append(
						$('<div>').attr("class", "col-sm-12").append(
								$('<h2>').attr("class", "title").text("Closed Meetings")
						)
				)
		);
		if(data.length > 0) {
			$.each(data, function(key, val) {
				$('#closed').append(meetingToRow(val));
			});
		} else {
			$('#closed').append(
					$('<div>').attr("class", "row").append(
							$('<div>').attr("class", "col-sm-12").append(
									$('<p>').attr("class", "text-centered").text("No past meetings")
							)
					)
			); 
		}
  	}).fail(function(data) {
  		alert(data.responseJSON.message);
  	});
}

function loadOpened() {
	$.ajax({
		type: 'GET',
		cache: false,
		url: '/meeting/search?closed=false'
	}).done(function(data) {
		if(data.length == 0) {
			$.ajax({
				type: 'GET',
				cache: false,
				url: '/meetingRoom'
			}).done(function(data) {
				$('#meetingRoom').empty();
				$.each(data, function(key, val) {
					$('#meetingRoom').append(
							$('<option>').attr("value", val.meetingRoomName).text(
									val.meetingRoomName + " (" + val.location + ") - "
									+ val.maxPlaces + " places"
							)
					);
				});
				$('#opened-title').text("Create Meeting");
				$('#addMeeting').show();
				$('#createMeeting').click(function() {
					var jsonData = {};
					var errorText = null;
					var dateRegexp = /^\d{4}-\d{2}-\d{2}$/;
					var timeRegexp = /^([0-1][0-9]|2[0-3]):([0-5][0-9])$/;
					$('#error').empty();
					jsonData['subject'] = $('#subject').val();
					if(jsonData['subject'] == null || jsonData['subject'].length == 0) {
						$('#error').text('Missing subject');
						return;
					}
					jsonData['date'] = $('#date').val();
					if(jsonData['date'] == null || !dateRegexp.test(jsonData['date']) || 
							isNaN(Date.parse(jsonData['date']))) {
						$('#error').text('Invalid date');
						return;
					} else if(jsonData['date'] < today()) {
						$('#error').text('Past date');
						return;
					}
					jsonData['startTime'] = $('#startTime').val();
					if(jsonData['startTime'] == null || !timeRegexp.test(jsonData['startTime'])) {
						$('#error').text('Invalid start time');
						return; 
					} else if(jsonData['date'] == today() && jsonData['startTime'] < now()) {
						$('#error').text('Past start time');
						return;
					}
					jsonData['endTime'] = $('#endTime').val();
					if(jsonData['endTime'] == null || !timeRegexp.test(jsonData['endTime'])) {
						$('#error').text('Invalid end time');
						return;
					}
					if(jsonData['endTime'] != null && jsonData['startTime'] != null &&
							jsonData['endTime'] <= jsonData['startTime']) {
						$('#error').text('End time must be after start time');
						return;
					}
					jsonData['meetingRoomName'] = $('#meetingRoom').val();
					$.ajax({
						type: 'POST',
						url: '/meeting',
						cache: false,
						data: JSON.stringify(jsonData),
						contentType: "application/json;charset=UTF-8"
					}).done(function(data) {
						$('#addMeeting')[0].reset();
						$('#addMeeting').hide();
						$('#createMeeting').unbind("click");
						loadData();
				  	}).fail(function(data) {
				  		alert(JSON.stringify(data));
				  	});
				});
			});
		} else if(data.length == 1) {
			$('#opened-title').text("Open Meeting");
			$('#addMeetingForm').hide();
			$('#opened').html(meetingDetailToHtml(data[0]));
			$('#opened').show();
		} else {
			alert("Error: more than 1 opened meeting!\n" + JSON.stringify(data))
		}
  	}).fail(function(data) {
  		alert(data.responseJSON.message);
  	});
}

function displayOverlay(html) {
	html.click(function(clickData) {
		clickData.stopPropagation();
	});
    $("<div id='overlay'></div>").click(removeOverlay)
    	.html(html).appendTo("body");
}

function removeOverlay(data) {
	$('#overlay').remove();
}

function formToJson(formId) {
	var formArray = $(formId).serializeArray();
	console.log(formArray);
	var returnArray = {};
	for(var i = 0; i < formArray.length; i++) {
		returnArray[formArray[i]['name']] = formArray[i]['value'];
  	}
  	return returnArray;
}

function meetingToRow(jsonMeeting) {
	var row = $('<div>').attr("class", "row").attr("title", "Click for details");
	row.click(jsonMeeting, function(clickData) {
		displayOverlay(meetingDetailToHtml(clickData.data));
	});
	var classStr = "col-sm-12 ";
	if(jsonMeeting.closed)
		classStr += "meeting meeting-closed";
	else
		classStr += "meeting meeting-opened";
	var col = $('<div>').attr("class", classStr).attr("id", jsonMeeting.meetingId);
	col.append(
		$('<h2>').attr("class", "text-centered").text(jsonMeeting.subject),
		$('<p>').append(
				$('<b>').text("When: "),
				$('<i>').text(jsonMeeting.date + " from " + jsonMeeting.startTime + " to " + jsonMeeting.endTime),
				$('<br>'),
				$('<b>').text("Where: "),
				$('<i>').text(jsonMeeting.meetingRoom.meetingRoomName + " (" + 
						jsonMeeting.meetingRoom.location + ") - " + jsonMeeting.meetingRoom.maxPlaces + " places")
		)
	);
	row.append(col);
	return row;
}

function participantToHtml(participant) {
	if(participant == null)
		return "No presenter selected";
	var desc = "";
	if(participant.title)
		desc += participant.title;
	if(participant.firstName)
		desc += (desc ? " " : "") + participant.firstName;
	if(participant.lastName)
		desc += (desc ? " " : "") + participant.lastName;
	return $('<span>').append(
			$('<b>').text(participant.emailAddress),
			$('<span>').text(desc ? " (" + desc + ")" : "")
	);	
}

function meetingDetailToHtml(jsonMeeting) {
	var html = $('<div>').attr("id", jsonMeeting.meetingId);
	if(jsonMeeting.closed)
		html.attr("class", "detail meeting-detail meeting-closed");
	else
		html.attr("class", "col-sm-12 meeting-detail meeting-opened");
	var participants = $('<ul>');
	$.each(jsonMeeting.participants, function(key, val) {
		participants.append(
				$('<li>').html(participantToHtml(val))
		);
	});
	var excluded = $('<ul>');
	$.each(jsonMeeting.excluded, function(key, val) {
		excluded.append(
				$('<li>').html(participantToHtml(val))
		);
	});
	var absents = $('<ul>');
	$.each(jsonMeeting.absents, function(key, val) {
		absents.append(
				$('<li>').html(participantToHtml(val))
		);
	});
	var container = $('<div>').attr("class", "container-fluid");
	var row = $('<div>').attr("class", "row");
	container.append(row);
	if(jsonMeeting.closed) {
		row.append(
			$('<button>').attr("class", "btn btn-default btn-danger col-sm-4 col-sm-offset-4")
				.append(
						$('<i>').attr("class" ,"fa fa-reply")
								.attr("aria-hidden", "true"),
						"&nbsp;Back to list"
				).click(jsonMeeting.meetingId, function(clickData) {
					clickData.stopPropagation();
					removeOverlay();
				})
		);
	} else {
		row.append(
				$('<div>').attr("class", "col-sm-4").append(
					$('<button>').attr("class", "btn btn-default btn-100w")
						.append(
								$('<i>').attr("class", "fa fa-times-circle fa-fw")
										.attr("aria-hidden", "true"),
								"&nbsp;Close meeting"
						).click(jsonMeeting.meetingId, function(clickData) {
							clickData.stopPropagation();
							$.ajax({
								type: 'POST',
								cache: false,
								url: '/meeting/' + clickData.data + '/close'
							}).done(function(data) {
								$('#' + clickData.data).remove();
								loadData();
						  	}).fail(function(data) {
						  		alert(JSON.stringify(data));
						  	});
						})
					)
		);
		row.append(
				$('<div>').attr("class", "col-sm-4").append(
					$('<button>').attr("class", "btn btn-default btn-100w")
						.append(
								$('<i>').attr("class" ,"fa fa-gears")
										.attr("aria-hidden", "true"),
								"&nbsp;Manage People"
					).click(jsonMeeting.meetingId, function(clickData) {
						clickData.stopPropagation();
						displayOverlay(
								$('<div>').attr("class", "manage-participants")
										.load("managePeople.html", function() {
											loadManageParticipantsData(clickData.data);
											addClickListener();
											$('#closeOverlay').click(function(clickData) {
												removeOverlay();
												loadData();
											});
										})
						);
					})
				)
		);
		row.append(
				$('<div>').attr("class", "col-sm-4").append(
					$('<button>').attr("class", "btn btn-default btn-100w")
					.append(
							$('<i>').attr("class" ,"fa fa-random")
									.attr("aria-hidden", "true"),
							"&nbsp;New Presenter"
				).click(jsonMeeting.meetingId, function(clickData) {
							clickData.stopPropagation();
							$.ajax({
								type: 'POST',
								cache: false,
								url: '/meeting/' + clickData.data + '/presenter'
							}).done(function(data) {
								$("#presenter").html(participantToHtml(data));
						  	}).fail(function(data) {
						  		$("#errorPresenter").text(data.responseJSON.message);
						  	});
						})
					)
		);
	}
	html.append(
		$('<h2>').attr("class", "text-centered").text(jsonMeeting.subject),
		$('<p>').append(
				$('<b>').text("When: "),
				$('<i>').text(jsonMeeting.date + " from " + jsonMeeting.startTime + " to " + jsonMeeting.endTime),
				$('<br>'),
				$('<b>').text("Where: "),
				$('<i>').text(jsonMeeting.meetingRoom.meetingRoomName + " (" + 
						jsonMeeting.meetingRoom.location + ") - " + jsonMeeting.meetingRoom.maxPlaces + " places"),
				$('<br>'),
				$('<p>').append(
						$('<b>').text("Participants: "),
						participants
				),
				$('<p>').append(
						$('<b>').text("Excluded: "),
						excluded
				),
				$('<p>').append(
						$('<b>').text("Absents: "),
						absents
				),
				$('<h3>').attr("class", "text-centered").append(
						$('<div>').text("Presenter: "),
						$('<div>').attr("id", "presenter").html(
								participantToHtml(jsonMeeting.presenter)
						)
				),
				container,
				$('<p>').append(
						$('<strong>').attr("id", "errorPresenter")
				)
		)
	);
	return html;
}

function now() {
	var today = new Date();
	var hh = today.getHours();
	var mm = today.getMinutes();
	if(hh < 10){
	    hh = '0' + hh;
	} 
	if(mm < 10){
	    mm = '0' + mm;
	} 
	return hh + ':' + mm;
}

function today() {
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth() + 1;
	var yyyy = today.getFullYear();
	if(dd < 10){
	    dd = '0' + dd;
	} 
	if(mm < 10){
	    mm = '0' + mm;
	} 
	return yyyy + '-' + mm + '-' + dd;
}